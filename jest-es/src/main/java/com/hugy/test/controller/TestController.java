package com.hugy.test.controller;

import com.hugy.entity.CloudPaper;
import com.hugy.test.entity.es.PageResult;
import com.hugy.test.entity.es.QueryParam;
import com.hugy.test.entity.es.Term;
import com.hugy.test.entity.es.TermTypeEnum;
import com.hugy.test.util.JestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class TestController {

    @GetMapping("/test01")
    @ResponseBody
    public PageResult<CloudPaper> test01(){
        QueryParam param = new QueryParam();
        param.addTerm(Term.build("fieldName", TermTypeEnum.LLIKE, "金融"));
        param.addTerm(Term.build("_Id","123"));
        PageResult<CloudPaper> result = JestUtils.getDocumentTest(param, CloudPaper.class);
        return result;
    }

    @GetMapping("/test02")
    public String test02(){
        return JestUtils.getIndexAlias(CloudPaper.class);
    }
}
