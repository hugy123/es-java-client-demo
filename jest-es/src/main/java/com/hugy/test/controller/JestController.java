package com.hugy.test.controller;

import com.hugy.test.entity.model.Post;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Count;
import io.searchbox.core.CountResult;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.indices.IndicesExists;
import io.searchbox.indices.aliases.AddAliasMapping;
import io.searchbox.indices.aliases.GetAliases;
import io.searchbox.indices.aliases.ModifyAliases;
import io.searchbox.indices.mapping.GetMapping;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class JestController {

    /*
        autowire形式 从spring容器中获取 jestClient
     */
    @Autowired
    public JestClient jestClient;
    /**
     * 查询文档
     */
    public void getDocumentTest() {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery("empNo", "123"));
        searchSourceBuilder.from(0);
        searchSourceBuilder.size(30);
        Search search = new Search.Builder(searchSourceBuilder.toString())
                // multiple index or types can be added.
                .addIndex("*dol*")
                .addType("cloud_logs")
                .build();
        try {
            SearchResult result = jestClient.execute(search);
            List<SearchResult.Hit<Post, Void>> hits = result.getHits(Post.class);
            List<Post> articles = result.getSourceAsObjectList(Post.class);
            System.out.println(articles);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 给索引添加别名
     */
    public void createAliasTest() {
        List<String> indics = new ArrayList<>();
        indics.add("posts");
        indics.add("postsss");
        try {
            AddAliasMapping addAliasMapping = new AddAliasMapping.Builder(indics, "mypost").build();
            JestResult jestResult = jestClient.execute(new ModifyAliases.Builder(addAliasMapping).build());
            System.out.println("result:" + jestResult.getJsonString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 根据index获取别名
     */
    @org.springframework.web.bind.annotation.GetMapping("/getAlias")
    public String getIndexAlias() {
        String index = "cloud_paper_alias";
        JestResult jestResult = null;
        try {
            jestResult = jestClient.execute(new GetAliases.Builder().addIndex(index).build());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jestResult.getJsonString();
    }

    /**
     * 判断index是否存在
     */
    public void indicesExists(){
        String index = "posoooots";
        IndicesExists indicesExists=new IndicesExists.Builder(index).build();
        JestResult jestResult=null;
        try{
            jestResult = jestClient.execute(indicesExists);
            System.out.println(jestResult.isSucceeded());
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * count
     * 必须要指定精确的index
     */
    public void countTest() throws IOException {
        Count count = new Count.Builder()
                .addIndex("cvf_2018-10-09")
                .addType("cloud_logs")
                .build();

        CountResult result = jestClient.execute(count);
        System.out.println(result.getCount());
    }

    public void getIndexList() throws Exception{
        GetMapping getMapping=new GetMapping.Builder().build();
        JestResult jestResult=jestClient.execute(getMapping);
        System.out.println(jestResult.getJsonString());
    }

}
