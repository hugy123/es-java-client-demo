package com.hugy.test.entity.es;

/**
 * 条件类型枚举
 *
 * @author YangChao
 * @create 2018-01-12 9:51
 **/
public enum TermTypeEnum {

    EQ, NE, EMPTY, NEMPTY, LIKE, NLIKE, LLIKE, RLIKE, GT, LT, GTE, LTE, IN, NIN, BETWEEN, NBETWEEN, NULL, NNULL, EXISTS, NEXISTS, QUERY_STRING, MATCH, MATCH_PHRASE, CONTAINS, WITHIN, DISJOINT, INTERSECTS;

}