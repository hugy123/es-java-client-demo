package com.hugy.test.entity.es;

import lombok.Data;

/**
 * @author YangChao
 * @create 2018-07-27 10:54
 **/
@Data
public class Sort {

    /**
     * 列名称
     */
    private String name;

    /**
     * 排序desc,asc
     *
     * @see SortEnum
     */
    private String order;

    public Sort() {
    }

    public Sort(String column) {
        this.name = column;
        this.order = SortEnum.DESC.getValue();
    }

    public Sort(String column, SortEnum order) {
        this.name = column;
        this.order = order.getValue();
    }

}