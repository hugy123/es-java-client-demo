package com.hugy.test.entity.es;

import lombok.Builder;
import lombok.Data;

/**
 * @author YangChao
 * @create 2019-03-27 11:05
 **/
@Data
@Builder
public class AggregationField {

    /**
     * 字段名称
     */
    private String field;

    /**
     * 聚合类型
     */
    private AggregationEnum type;

    /**
     * 别名
     */
    private String alias;

}