package com.hugy.test.entity.es;

/**
 * 枚举接口
 *
 * @author YangChao
 * @create 2017-10-26 16:32
 **/
public interface Enum<V, T> {

    /**
     * 获取枚举值
     *
     * @return
     */
    V getValue();

    /**
     * 获取枚举名称
     *
     * @return
     */
    T getText();

}
