package com.hugy.test.entity.es;


import lombok.Getter;

/**
 * @author YangChao
 * @create 2019-03-27 11:06
 **/
@Getter
public enum AggregationEnum implements Enum<String, String> {

    /**
     * count
     */
    COUNT("count", "汇总"),

    SUM("sum", "累加"),

    AVG("avg", "平均值"),

    MAX("max", "最大值"),

    MIN("min", "最小值"),

    STATS("stats", "状态");

    private final String value;
    private final String text;

    AggregationEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

}