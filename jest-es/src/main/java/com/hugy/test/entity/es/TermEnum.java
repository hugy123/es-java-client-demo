package com.hugy.test.entity.es;

/**
 * 字段操作类型枚举
 *
 * @author YangChao
 * @create 2018-01-12 10:00
 **/
public enum TermEnum {

    AND, OR

}