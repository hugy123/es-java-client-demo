package com.hugy.test.entity.es;

import lombok.Getter;

/**
 * 排序枚举
 *
 * @author YangChao
 * @create 2018-01-12 9:27
 **/
@Getter
public enum SortEnum implements Enum<String, String> {

    DESC("desc", "降序"),

    ASC("asc", "升序");

    private final String value;
    private final String text;

    SortEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

}