package com.hugy.test.entity.es;


import lombok.Data;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @author YangChao
 * @create 2017-11-23 8:58
 **/
//@ApiModel(description = "分页结果")
@Data
public class PageResult<E> implements Serializable {

    public static <E> PageResult<E> empty() {
        return new PageResult<>(0, Collections.emptyList());
    }

    public static <E> PageResult<E> build(int total, List<E> list) {
        return new PageResult<>(total, list);
    }

//    @ApiModelProperty("数据总条数")
    private long total = 0;

//    @ApiModelProperty("数据总页数")
    private long totalPages = 0;

//    @ApiModelProperty("第几页 从0开始")
    private long pageNo = 0;

//    @ApiModelProperty("每页显示记录条数")
    private long pageSize = 0;

//    @ApiModelProperty("数据集合")
    private List<E> data;

    public PageResult() {
    }

    public PageResult(long total, List<E> data) {
        this.total = total;
        this.data = data;
    }

    public void setTotal(long total) {
        this.total = total;
        if (this.pageSize > 0) {
            this.totalPages = this.total % this.pageSize == 0 ? this.total / this.pageSize : this.total / this.pageSize + 1;
        }
    }

}
