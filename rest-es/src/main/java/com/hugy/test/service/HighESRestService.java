package com.hugy.test.service;

import com.hugy.test.model.Person;
import com.hugy.test.util.RestHlvEsUtils;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class HighESRestService {

    @Autowired
    RestHlvEsUtils restHlvEsUtils;

    /**
     * 往索引添加数据
     *
     * @return
     */
    public String add() {
        String Id = null;
        String index = "person";
        String id = "10001";
        try {
            XContentBuilder builder = XContentFactory.jsonBuilder()
                    .startObject()
                    .field("id", "10001")
                    .field("name", "张三")
                    .field("age", "28")
                    .field("country", "中国")
                    .field("addr", "广东深圳")
                    .field("data", "2020-01-15 20:47:20")
                    .field("birthday", "1992-01-01")
                    .endObject();
            Id = restHlvEsUtils.addData(builder, index, id);
        } catch (IOException e) {
            log.error("索引:{},id:{},添加数据失败", index, id);
        }
        return Id;
    }

    /**
     * 更新指定id的文档数据
     *
     * @return
     */
    public String update() {
        String Id = null;
        String index = "person";
        String id = "10001";
        String type = "10001";
        try {
            XContentBuilder builder = XContentFactory.jsonBuilder()
                    .startObject()
                        .field("id", "10001")
                        .field("name", "李四")
                        .field("age", "30")
                        .field("country", "中国")
                        .field("addr", "广东深圳")
                        .field("data", "2020-01-15 20:47:20")
                        .field("birthday", "1990-01-01")
                    .endObject();
            Id = restHlvEsUtils.updateData(builder, index, type, id);
        } catch (IOException e) {
            log.error("索引:{},id:{},添加数据失败", index, id);
        }
        return Id;
    }

    /**
     * 批量插入数据
     *
     * @return
     */
    public String insertBatch() {
        String index = "person";
        List<Person> entityList = new ArrayList<>(100);
        for (int i = 0; i < 100; i++) {
            Person person = new Person();
        }
        return restHlvEsUtils.insertBatch(index, entityList);
    }

    /**
     * 根据条件删除
     */
    public void delete() {
        String index = "person";
        QueryBuilder queryBuilder = QueryBuilders.matchQuery("age", "30");
        restHlvEsUtils.deleteByQuery(index, queryBuilder);

    }

    /**
     * 根据id删除文档
     *
     * @return
     */
    public String deleteById() {
        String s;
        String index = "person";
        String type = "1001";
        String id = "1001";
        s = restHlvEsUtils.deleteById(index, type, id);
        return s;
    }

    /**
     * 根据条件查询
     *
     * @return
     */
    public List searchData() {
        List<Map<String, Object>> list = new ArrayList<>();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        String[] fields = {"name", "addr", "birthday", "id"};
        //需要返回和不返回的字段，可以是数组也可以是字符串
        sourceBuilder.fetchSource(fields, null);
        //设置根据哪个字段进行排序查询
        sourceBuilder.sort(new FieldSortBuilder("birthday").order(SortOrder.DESC));
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        //添加查询条件
        builder.must(QueryBuilders.matchQuery("country", "中国"));
        list = restHlvEsUtils.SearchDataPage("person", 1, 10, sourceBuilder, builder);
        return list;
    }
}
