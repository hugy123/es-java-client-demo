package com.hugy.test.controller;

import com.hugy.test.service.HighESRestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/*
    官方文档
   https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.6/java-rest-high.html
 */
@RestController
@RequestMapping("/es/high")
@Slf4j
public class HighRestTestController {

    @Resource
    private HighESRestService service;

    @PostMapping("/add")
    public String add() {
        return service.add();
    }

    @PostMapping("/update")
    public String update() {
        return service.update();
    }

    @PostMapping("/insertBatch")
    public String insertBatch() {
        return service.insertBatch();
    }

    @PostMapping("/deleteByQuery")
    public void delete() {
        service.delete();
    }

    @PostMapping("/deleteById")
    public String deleteById() {
        return service.deleteById();
    }

    @PostMapping("/searchData")
    public List searchData() {
        return service.searchData();
    }
}
