package com.hugy.test.controller;

import com.alibaba.fastjson.JSONObject;
import com.hugy.test.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static com.hugy.test.model.Constants.PARAM;

/*
    官方文档
    https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.6/java-rest-low.html
 */
@RestController
@RequestMapping("/es/low")
@Slf4j
public class LowRestTestController {

    @Qualifier("myRestClient")
    @Autowired
    private RestClient client;

    private final String body = PARAM.getIndex() + "/" + PARAM.getType() + "/" + PARAM.getId();

    @GetMapping("/searchDoc")
    public JSONObject getDataById(){
        Request request = new Request("GET", body);
        request.addParameter("pretty", "true");
        JSONObject jsonObject = new JSONObject();
        try {
            Response response = client.performRequest(request);
            String responseBody = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONObject json = JSONObject.parseObject(responseBody);
            //获取我们需要的内容
            Object source = json.get("_source");
            jsonObject = JSONObject.parseObject(source.toString());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return jsonObject;
    }

    /**
     * 添加文档
     */
    @PostMapping("/addDoc")
    public ResponseEntity add() {
        //以bean的id为该文档的id,以便查询
        String responseBody = "";
        try {
            Request request = new Request("POST", body);
            JSONObject jsonObject = (JSONObject) JSONObject.toJSON(new Person());
            //设置请求体并指定ContentType和编码格式
            request.setEntity(new NStringEntity(jsonObject.toString(), "UTF-8"));
//            Response response = client.performRequest(request);
            //获取响应体
//            responseBody = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    /**
     * 根据id更新文档内容
     */
    @PostMapping("/updateDoc")
    public ResponseEntity update() {
        String responseBody = "";
        try {
            //构造http请求
            Request request = new Request("POST", body);
            JSONObject json = (JSONObject) JSONObject.toJSON(new Person());
            JSONObject jsonObject = new JSONObject();
            //将数据由"doc"包住，以便内部识别
            jsonObject.put("doc", json);
            request.setEntity(new NStringEntity(jsonObject.toString(), "UTF-8"));
//            Response response = client.performRequest(request);
            //获取响应体
//            responseBody = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    /**
     * 根据id删除文档
     */
    @PostMapping("/delDoc")
    public ResponseEntity delete() {
        String responseBody = "";
        try {
            Request request = new Request("DELETE", body);
            // 执行HTTP请求
//            Response response = client.performRequest(request);
            // 获取结果
//            responseBody = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }
}
