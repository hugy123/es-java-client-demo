package com.hugy.test.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Person implements Serializable {
    private String id = "1234567890";
    private String name ="朱元璋";
    private String addr ="安徽凤阳";
    private String birthday = "1328-10-29";
}
