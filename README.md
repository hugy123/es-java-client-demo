# es-java-clientDemo

#### 介绍
四种操作java与ES整合操作方式

#### 软件架构
软件架构说明

1.JestClient

2.RestClient
REST Client 有一个高版本，和一个低版本。主要是用来适应不同的 ES版本的。

3.TransportClient
这种方式，官方已经明确表示在ES 7.0版本中将弃用TransportClient客户端，且在8.0版本中完全移除它，我在学习ES时，版本已经是6.4.0了。
推荐博客：
https://www.cnblogs.com/a-du/p/9172272.html
https://blog.csdn.net/u011781521/article/details/77891192
4.Spring-data-es



