package com.hugy.test.controller;

import com.hugy.entity.CloudPaper;
import com.hugy.test.dao.CloudPaperDao;
import com.hugy.test.service.CloudPaperService;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Pageable;
import java.util.List;

@RequestMapping("/es/data")
@RestController
public class DataController {

    @Autowired
    private CloudPaperService cloudPaperService;

    @Autowired
    private CloudPaperDao cloudPaperDao;

    @GetMapping("/test")
    public ResponseEntity<List<CloudPaper>> getData(){
        String query = "{\"query\":{\"bool\":{\"must\":[{\"term\":{\"scholarName\":\"王峰\"}}],\"must_not\":[],\"should\":[]}},\"from\":0,\"size\":10,\"sort\":[],\"aggs\":{}}";
        List<CloudPaper> search = cloudPaperService.search(query);
        return ResponseEntity.status(200).body(search);
    }

    @GetMapping("/test2")
    public ResponseEntity<List<CloudPaper>> getData2(@PageableDefault(page = 0, value = 3) Pageable pageable){
        // 查询所有的
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        // 模糊查询 一定要ik中文
        TermQueryBuilder matchQuery = QueryBuilders.termQuery("name", "Ajay Kumar");
        boolQuery.must(matchQuery);

        Page<CloudPaper> page = cloudPaperDao.search(boolQuery, pageable);
        return ResponseEntity.status(200).body(page.getContent());
    }
}
