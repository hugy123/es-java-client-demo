package com.hugy.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
//@EnableElasticsearchRepositories(basePackages = "com.hugy.test.dao")
public class DataApp {

    public static void main(String[] args) {
        SpringApplication.run(DataApp.class, args);
    }
}
