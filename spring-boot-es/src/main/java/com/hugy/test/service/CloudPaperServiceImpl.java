package com.hugy.test.service;

import com.hugy.entity.CloudPaper;
import com.hugy.test.dao.CloudPaperDao;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class CloudPaperServiceImpl implements CloudPaperService {

    @Autowired
    private CloudPaperDao cloudPaperDao;

    @Override
    public List<CloudPaper> search(String searchContent) {
        QueryStringQueryBuilder builder = new QueryStringQueryBuilder(searchContent);
        System.out.println("查询的语句:"+builder);
        Iterable<CloudPaper> searchResult = cloudPaperDao.search(builder);
        Iterator<CloudPaper> iterator = searchResult.iterator();
        List<CloudPaper> list=new ArrayList<CloudPaper>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }
}
