package com.hugy.test.service;

import com.hugy.entity.CloudPaper;

import java.util.List;

public interface CloudPaperService {
    List<CloudPaper> search(String searchContent);
}
