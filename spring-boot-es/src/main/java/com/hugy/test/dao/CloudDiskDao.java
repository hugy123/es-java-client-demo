package com.hugy.test.dao;

import com.hugy.test.entity.CloudDiskEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface CloudDiskDao extends ElasticsearchRepository<CloudDiskEntity, String> {

}
