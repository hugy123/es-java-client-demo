package com.hugy.test.dao;

import com.hugy.entity.CloudPaper;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CloudPaperDao extends ElasticsearchRepository<CloudPaper, Long> {
}