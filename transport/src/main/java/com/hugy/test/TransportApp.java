package com.hugy.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransportApp {
    public static void main(String[] args) {
        SpringApplication.run(TransportApp.class, args);
    }
}
