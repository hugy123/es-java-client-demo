package com.hugy.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

/**
 * @author YangChao
 * @date 2019-03-21 14:14:30
 */
@Data
@Document(indexName = "cloud_paper",type = "cloud_paper_alias")
public class CloudPaper extends BaseEntity {
    private String type;

    private String title;

    private String abst;

    private String venue;

    private Integer year;

    private String lang;

    private String date;

    private Integer citationNum;

    private List<String> keywords;

    private List<String> fields;

}
